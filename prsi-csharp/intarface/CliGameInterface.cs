﻿using src.card;
using src.player;
using src.translations;

namespace src.intarface;

public class CliGameInterface : IGameInterface
{
    private readonly TextReader _cliReader;
    private readonly TextWriter _cliWriter;
    private readonly Translation _translation;

    public CliGameInterface(Translation translation)
    {
        _cliReader = Console.In;
        _cliWriter = Console.Out;
        _translation = translation;
    }

    private void ClearConsole()
    {
        
//        cliWriter.Write("\033[H\033[2J");
//        cliWriter.Flush();

        _cliWriter.WriteLine("-------------------------------------------------------------------------");
        _cliWriter.Flush();
    }

    public void GameStart()
    {
        ClearConsole();

        _cliWriter.WriteLine(_translation.GetTranslation("game_start"));
        _cliWriter.WriteLine();
        _cliWriter.Write(_translation.GetTranslation("prompt_for_continue"));
        _cliWriter.Flush();

        try {
            _cliReader.Read();
        } catch (IOException e) {
            _cliWriter.WriteLine(e.StackTrace);
        }
    }

    public Card? LetPlayerSelectCard(Player player, Card card, CardColor? colorOnChanger, int? overChargeCount)
    {
         var isSpecial = card.IsSpecial && overChargeCount != null;

        _cliWriter.WriteLine();
        if(card.IsSuperSpecial && colorOnChanger != null) {
            _cliWriter.Write(_translation.GetTranslation("current_color"));
            _cliWriter.WriteLine(":");
            _cliWriter.WriteLine(_translation.GetCardColorName(colorOnChanger.Value));
        } else {
            _cliWriter.Write(_translation.GetTranslation("current_card"));
            _cliWriter.WriteLine(":");
            _cliWriter.WriteLine(_translation.GetCardName(card));
        }
        _cliWriter.WriteLine();

        var cardsForSelect = new List<Card>();
        var remainingCards = new List<Card>();

        if(card.IsSuperSpecial) {
            foreach (var card1 in player.Deck) {
                if(card1.Color == colorOnChanger) {
                    cardsForSelect.Add(card1);
                } else {
                    remainingCards.Add(card1);
                }
            }
        } else if(isSpecial) {
            foreach (var card1 in player.Deck) {
                if(card1.Type == card.Type) {
                    cardsForSelect.Add(card1);
                } else {
                    remainingCards.Add(card1);
                }
            }
        } else {
            foreach (var card1 in player.Deck) {
                if(card1.IsSuperSpecial || card.CompareTo(card1) == 0) {
                    cardsForSelect.Add(card1);
                } else {
                    remainingCards.Add(card1);
                }
            }
        }

        if(cardsForSelect.Count > 0) {
            _cliWriter.Write(_translation.GetTranslation("cards_for_select"));
            _cliWriter.WriteLine(":");

            for (int i = 0; i < cardsForSelect.Count; i++) {
                _cliWriter.WriteLine($"{i}) {_translation.GetCardName(cardsForSelect[i])}");
            }

            _cliWriter.WriteLine();


            if(remainingCards.Count > 0) {
                _cliWriter.Write(_translation.GetTranslation("remaining_cards"));
                _cliWriter.WriteLine(":");

                foreach (var remainingCard in remainingCards) {
                    _cliWriter.WriteLine($"\t{_translation.GetCardName(remainingCard)}");
                }
            }

            _cliWriter.WriteLine();

            _cliWriter.WriteLine(_translation.GetTranslation("select_card"));
            _cliWriter.Write(": ");
            _cliWriter.Flush();

            try {
                var response = _cliReader.ReadLine();

                while (string.IsNullOrEmpty(response))
                {
                    response = _cliReader.ReadLine();
                }

                return cardsForSelect[int.Parse(response)];
            } catch (IOException) {}
        } else {
            _cliWriter.WriteLine(_translation.GetTranslation(isSpecial ? "no_cards_to_play_on_overcharge" : "no_cards_to_play"));

            _cliWriter.WriteLine();

            _cliWriter.Write(_translation.GetTranslation("remaining_cards"));
            _cliWriter.WriteLine(":");

            foreach (var remainingCard in player.Deck) {
                _cliWriter.WriteLine($"\t{_translation.GetCardName(remainingCard)}");
            }

            _cliWriter.WriteLine();

            _cliWriter.WriteLine(_translation.GetTranslation("prompt_for_continue"));
            _cliWriter.Flush();

            try {
                _cliReader.Read();
            } catch (IOException) {}
        }

        return null;
    }

    public void ShowFirstCard(Card currentCard)
    {
    }

    public void PlaceCard(Card selectedCard)
    {
    }

    public void AddCardToDeck(Player player, Card retrievedCard)
    {
    }

    public CardColor? LetPlayerSelectColor()
    {
        _cliWriter.WriteLine();

        _cliWriter.Write(_translation.GetTranslation("colors_for_select"));
        _cliWriter.WriteLine(":");

        var colors = Enum.GetValues<CardColor>();

        for (var i = 0; i < colors.Length; i++) {
            _cliWriter.WriteLine($"{i}) {_translation.GetCardColorName(colors[i])}");
        }

        _cliWriter.WriteLine();

        _cliWriter.WriteLine(_translation.GetTranslation("select_color"));
        _cliWriter.Write(": ");
        _cliWriter.Flush();

        try {
            var response = _cliReader.ReadLine();

            while (string.IsNullOrEmpty(response))
            {
                response = _cliReader.ReadLine();
            }

            return colors[int.Parse(response)];
        } catch (IOException e) {
            _cliWriter.WriteLine(e.StackTrace);
        }

        return null;
    }

    public void PlayerRetrievedOverChargedCards(Player player, int count)
    {
    }

    public void GameOver(Players players, int round)
    {
        ClearConsole();

        _cliWriter.WriteLine(_translation.GetTranslation("game_over"));
        _cliWriter.WriteLine();
        _cliWriter.Write(_translation.GetTranslation("winner_is"));

        var winners = players.Winners;

        if(winners.Count > 1) {
            var i = 1;

            while(winners.Count > 0) {
                _cliWriter.WriteLine($"{i++}. ${winners.Dequeue().Name}");
            }
        } else {
            _cliWriter.WriteLine(winners.Peek().Name);
        }

        _cliWriter.Flush();
    }

    public void CurrentPlayer(Player player)
    {
        ClearConsole();

        _cliWriter.Write(_translation.GetTranslation("player"));
        _cliWriter.Write(" ");
        _cliWriter.Write(player.Name);
        _cliWriter.Write(" ");
        _cliWriter.WriteLine(_translation.GetTranslation("plays"));
        _cliWriter.Flush();
    }
}