﻿using src;
using src.intarface;
using src.player;
using src.translations;

Translation lang = new CzechTranslation();
IGameInterface gameInterface = new CliGameInterface(lang);
var players = new HashSet<Player>
{
    new("Tomáš1"),
    new("Tomáš2")
};

var game = new Game(players, gameInterface);

game.PrepareGame();
game.StartGame();