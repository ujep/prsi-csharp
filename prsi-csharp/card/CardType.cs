﻿namespace src.card;

public enum CardType
{
    Svrsek, Menic, Kral, Eso, Seven, Eight, Nine, X
}