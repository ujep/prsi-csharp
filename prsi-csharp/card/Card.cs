﻿using System.Collections.Immutable;

namespace src.card;

public class Card : IComparable<Card>
{
    private readonly CardColor _color;
    private readonly CardType _type;

    private Card(CardColor color, CardType type)
    {
        _color = color;
        _type = type;
    }

    public CardColor Color => _color;
    public CardType Type => _type;

    public bool IsSuperSpecial => _isCardTypeSuperSpecial(_type);
    public bool IsSpecial => _isCardTypeSpecial(_type);

    public int CompareTo(Card? other)
    {
        if (other == null) return -1;
        return (other._type == _type || other._color == _color) ? 0 : -1;
    }
    
    static bool _isCardTypeSuperSpecial(CardType type) {
        return type == CardType.Menic;
    }

    static bool _isCardTypeSpecial(CardType type) {
        return type is CardType.Seven or CardType.Eso;
    }

    public class Cards {
        public static IList<Card> AllCards { get; } = _createCards();
        
        private static IList<Card> _createCards()
        {
            var types = Enum.GetValues<CardType>();

            return Enum
                .GetValues<CardColor>()
                .SelectMany(c => types.Select(t => new Card(c, t)))
                .ToImmutableList();
        }
    }
}