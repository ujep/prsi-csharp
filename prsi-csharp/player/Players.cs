﻿using System.Collections.Immutable;

namespace src.player;

public class Players
{
    private readonly ISet<Player> _players;
    private readonly ISet<Player> _remainingPlayers;
    private readonly Queue<Player> _winners;

    public Players(ISet<Player> players)
    {
        _players = players.ToImmutableHashSet();
        _remainingPlayers = players.ToHashSet();
        _winners = new Queue<Player>();
    }
    
    public void ClearPlayersDecks() {
        foreach(var p in _players)
        {
            p.ClearDeck();
        }
    }
    
    public void PlayerEnded(Player player) {
        if(_remainingPlayers.Contains(player)) {
            _remainingPlayers.Remove(player);

            _winners.Enqueue(player);
        }
    }
    
    public ISet<Player> RemainingPlayers => ImmutableHashSet.CreateRange(_remainingPlayers);
    public int RemainingPlayerCount => _remainingPlayers.Count;
    public Queue<Player> Winners => _winners;
    public ISet<Player> AllPlayers => _players;
    public bool IsEnd => RemainingPlayerCount == 1;
}