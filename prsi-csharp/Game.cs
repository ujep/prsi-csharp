﻿using src.card;
using src.intarface;
using src.player;

namespace src;

public class Game
{
    private const int CardsPerPlayer = 4;

    private readonly Deck _deck;
    private readonly Players _players;
    private readonly IGameInterface _gameInterface;

    private CardColor? _colorOnChanger;
    private int? _overChargeCount;

    private int _round;
    private bool _gameRunning;
    
    public Game(ISet<Player> players, IGameInterface gameInterface)
    {
        _players = new Players(players);
        _gameInterface = gameInterface;
        _deck = new Deck();
    }
    
    public void PrepareGame() {
        _deck.SetupDeck();
        _players.ClearPlayersDecks();

        for (var i = 0; i < CardsPerPlayer; i++) {
            foreach (var player in _players.AllPlayers) {
                player.AddCardToDeck(_deck.RetrieveCard());
            }
        }
    }
    
    public void StartGame() {
        if(!_gameRunning)
        {
          _gameRunning = true;

          var t = new Thread(Run);
          t.Name = "Game";

          t.Start();
        }
    }

    private void Run()
    {
      _gameInterface.GameStart();
      var currentCard = _deck.RetrieveCard();

      if (currentCard.Type == CardType.Menic)
      {
        _colorOnChanger = currentCard.Color;
      }
      else if (currentCard.Type == CardType.Eso)
      {
        _overChargeCount = 0;
      }
      else if (currentCard.Type == CardType.Seven)
      {
        _overChargeCount = 1;
      }

      _gameInterface.ShowFirstCard(currentCard);

      while (_players.RemainingPlayerCount > 1)
      {
        foreach (var player in _players.RemainingPlayers)
        {
          _gameInterface.CurrentPlayer(player);

          if (currentCard.IsSpecial && _overChargeCount != null)
          {
            var selectedCard = _gameInterface.LetPlayerSelectCard(
              player, currentCard, null, _overChargeCount);

            if (selectedCard != null)
            {
              if (selectedCard.Type == CardType.Seven)
              {
                _overChargeCount = _overChargeCount! + 1;
              }

              player.RemoveCardFromDeck(selectedCard);
              _deck.AddPlacedCard(selectedCard);
              _gameInterface.PlaceCard(selectedCard);
              currentCard = selectedCard;

              if (!player.IsPlaying)
              {
                _players.PlayerEnded(player);
              }
            }
            else
            {
              if (currentCard.Type == CardType.Seven)
              {
                var cardCount = 2 * _overChargeCount.Value;

                player.AddCardsToDeck(_deck.RetrieveCards(cardCount));
                _gameInterface.PlayerRetrievedOverChargedCards(player, cardCount);
              }

              _overChargeCount = null;
            }
          }
          else
          {
            var selectedCard = _gameInterface.LetPlayerSelectCard(
              player, currentCard, _colorOnChanger, _overChargeCount);

            if (selectedCard != null)
            {
              if (selectedCard.Type == CardType.Menic)
              {
                _colorOnChanger = _gameInterface.LetPlayerSelectColor();
              }
              else if (selectedCard.Type == CardType.Seven)
              {
                _overChargeCount = 1;
              }
              else if (selectedCard.Type == CardType.Eso)
              {
                _overChargeCount = 0;
              }

              player.RemoveCardFromDeck(selectedCard);
              _deck.AddPlacedCard(selectedCard);
              _gameInterface.PlaceCard(selectedCard);
              currentCard = selectedCard;

              if (!player.IsPlaying)
              {
                _players.PlayerEnded(player);
              }
            }
            else
            {
              var retrievedCard = _deck.RetrieveCard();

              player.AddCardToDeck(retrievedCard);
              _gameInterface.AddCardToDeck(player, retrievedCard);
            }
          }

          if (_players.IsEnd)
          {
            break;
          }
        }

        _round++;
      }

      _gameInterface.GameOver(_players, _round);
    }
}