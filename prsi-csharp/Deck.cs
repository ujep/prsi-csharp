﻿using src.card;

namespace src;

public class Deck
{
    private readonly IList<Card> _cards;
    private readonly List<Card> _remainingCards;
    private readonly List<Card> _placedCards;

    public Deck()
    {
        _cards = Card.Cards.AllCards.ToList().AsReadOnly();
        _remainingCards = new List<Card>();
        _placedCards = new List<Card>();
    }
    
    public void SetupDeck() {
        _placedCards.Clear();

        _remainingCards.Clear();
        _remainingCards.AddRange(_cards);
        _remainingCards.Shuffle();
    }

    public Card RetrieveCard()
    {
        if(_remainingCards.Count > 0)
        {
            var card = _remainingCards[0];
            _remainingCards.RemoveAt(0);
            
            return card;
        }

        Reroll();
        return RetrieveCard();
    }
    
    public List<Card> RetrieveCards(int count) {
        var array = new List<Card>();

        for (var i = 0; i < count; i++) {
            array.Add(RetrieveCard());
        }

        return array;
    }
    
    private void Reroll() {
        _remainingCards.AddRange(_placedCards);
        _remainingCards.Reverse();
        
        _placedCards.Clear();
    }
    
    public void AddPlacedCard(Card card) {
        _placedCards.Add(card);
    }
}