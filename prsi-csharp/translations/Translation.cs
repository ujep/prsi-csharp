﻿using src.card;

namespace src.translations;

public abstract class Translation
{
    private readonly IDictionary<Card, String> _cardTranslations;
    private readonly IDictionary<CardColor, String> _cardColorTranslations;
    private readonly IDictionary<String, String> _stringTranslations;
    private readonly String _internationalLanguageName;
    private readonly String _languageName;

    protected Translation(string internationalLanguageName, string languageName)
    {
        _internationalLanguageName = internationalLanguageName;
        _languageName = languageName;

        _cardTranslations = CardTranslation();
        _cardColorTranslations = CardColorTranslation();
        _stringTranslations = StringTranslation();
    }

    protected abstract IDictionary<Card, string> CardTranslation();
    protected abstract IDictionary<CardColor, string> CardColorTranslation();
    protected abstract IDictionary<string, string> StringTranslation();
    
    public string GetCardName(Card card) => _cardTranslations[card];
    public string GetCardColorName(CardColor card) => _cardColorTranslations[card];
    public string? GetTranslation(string key) => _stringTranslations[key];
}